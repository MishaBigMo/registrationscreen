//
//  ViewController.swift
//  registrationScreen
//
//  Created by Михаил on 16/09/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Properties
    var keyboardIsShow = false
    var mails: [String] = ["@gmail.com", "@yahoo.com", "@inbox.com", "@mail.ru", "@hotmail.com", "@msn.com", "@yandex.ru", "@outlook.com", "@me.com"]
    
    
    // MARK: - Outlets
    @IBOutlet weak var uncorrectEmailFormLabel: UILabel!
    @IBOutlet weak var uncorrectPasswordFormLabel: UILabel!
    @IBOutlet weak var label: UIImageView!
    @IBOutlet weak var buttonContraint: NSLayoutConstraint!
    @IBOutlet weak var emailHelperButton: UIButton!
    @IBOutlet weak var loginTextFiled: UITextField!
    @IBOutlet weak var loginSeparatorView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordSeparatorLineView: UIView!
    @IBOutlet weak var bottonConstrants: NSLayoutConstraint!
    @IBOutlet weak var registrationButton: UIButton! {
        didSet {
            registrationButton.layer.cornerRadius = 6
            registrationButton.layer.masksToBounds = true
        }
    }
    
    
    // MARK: - Actions
    @IBAction func emailHelperTapped(_ sender: UIButton) {
        if var login = loginTextFiled.text, let emailHelper = sender.titleLabel?.text {
            if let dotRange = login.range(of: "@") {
                login.removeSubrange(dotRange.lowerBound..<login.endIndex)
            }
            
            loginTextFiled.text = "\(login)\(emailHelper)"
            emailHelperButton.setTitle("", for: .normal)
            emailHelperButton.isEnabled = true
        }
    }
    
    @IBAction func showOrHidePasswordTapped(_ sender: UIButton) {
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func loginTextFieldEditingChanged(_ sender: UITextField) {
        helperLoginForm(sender: sender)
        print(sender.text)
    }
    
    @IBAction func registrationTapped(_ sender: Any) {
       checkCorrectLoginAndPasswordForm()
    }
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createGradientLayerForButton()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    // MARK: - Function
    func helperLoginForm(sender: UITextField) {
        let result = mails.filter({ $0.contains(sender.text!.suffix(2))})
        if result.first != nil {
            emailHelperButton.setTitle(result.first, for: .normal)
        }
    }
    
    func createGradientLayerForButton() {
        loginTextFiled.keyboardType = .default
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = registrationButton.frame.size
        let turquoiseColor = UIColor(red: 10/255, green: 196/255, blue: 186/255, alpha: 1.0)
        let greenblueColor = UIColor(red: 43/255, green: 218/255, blue: 142/255, alpha: 1.0)
        gradientLayer.colors = [turquoiseColor.cgColor, greenblueColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        registrationButton.layer.addSublayer(gradientLayer)
        
        self.view.addSubview(registrationButton)
    }
    
    func checkCorrectLoginAndPasswordForm() {
        //password
        if passwordTextField.text!.count < 8 {
            uncorrectPasswordFormLabel.text = "Пароль должен содержать миниум 8 символов!"
            passwordSeparatorLineView.backgroundColor = .red
        } else if passwordTextField.text!.count > 18 {
            uncorrectPasswordFormLabel.text = "Пароль должен содержать максимум 18 символов!"
            passwordSeparatorLineView.backgroundColor = .red
        } else {
            passwordSeparatorLineView.backgroundColor = .green
            uncorrectPasswordFormLabel.text = ""
        }
        
        //login
        if loginTextFiled.text?.isEmail == false {
            loginSeparatorView.backgroundColor = .red
            uncorrectEmailFormLabel.text = "Логин имеет неправильную форму"
        } else {
            if (loginTextFiled.text?.hasSuffix(".com"))! || (loginTextFiled.text?.hasSuffix(".ru"))! || (loginTextFiled.text?.hasSuffix(".org"))! || (loginTextFiled.text?.hasSuffix(".edu"))! || (loginTextFiled.text?.hasSuffix(".net"))! || (loginTextFiled.text?.hasSuffix(".info"))! || (loginTextFiled.text?.hasSuffix(".biz"))! || (loginTextFiled.text?.hasSuffix(".io"))! || (loginTextFiled.text?.hasSuffix(".co"))! || (loginTextFiled.text?.hasSuffix(".gov"))! || (loginTextFiled.text?.hasSuffix(".co.uk"))! || (loginTextFiled.text?.hasSuffix(".by"))! {
                loginSeparatorView.backgroundColor = .green
                uncorrectEmailFormLabel.text = ""
            } else {
                loginSeparatorView.backgroundColor = .red
                uncorrectEmailFormLabel.text = "неправильно доменное имя"
            }
        }
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        if keyboardIsShow == false {
            var userInfo = notification.userInfo!
            let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
            let changeInHeight = keyboardFrame.height + 80
            
            UIView.animate(withDuration: 0.25, animations: {
                self.label.alpha = 0
            }) { (finished) in
                self.label.isHidden = true
            }
            UIView.animate(withDuration: animationDurarion) {
                self.buttonContraint.constant = CGFloat(changeInHeight)
            }
        }
        keyboardIsShow = true
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        if keyboardIsShow == true {
        var userInfo = notification.userInfo!
        let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let animationDurarion = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let changeInHeight = 180 //(keyboardFrame.height + 80)
        
        UIView.animate(withDuration: 0.3) {
            self.label.isHidden = false
            self.label.alpha = 1
        }
        
        UIView.animate(withDuration: animationDurarion) {
            self.buttonContraint.constant = CGFloat(changeInHeight)
            
        }
        }
        keyboardIsShow = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if keyboardIsShow == true {
        self.view.endEditing(true)
        }
    }
    
}





extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension String {
    private static let __firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
    private static let __serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
    private static let __emailRegex = __firstpart + "@" + __serverpart + "[A-Za-z]{2,6}"
    
    public var isEmail: Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", type(of:self).__emailRegex)
        return predicate.evaluate(with: self)
    }
}
